function endTestSuite
    if status is-interactive
        echo "ERROR: Cannot use fishTest in an interactive terminal."
        echo "Type fishTest --help for more information."
        return 1
    end
    
    if ! set -q __test_suite_active
        status -t
        echo "ERROR: There is no matching testSuite for endSuite."
        echo -e "Cannot use \033[1mendTestSuite\033[0m without using \033[1mtestSuite\033[0m first."
        echo "Type endTestSuite --help for more information."
        return 1
    end
    
    if test "$__test_suite_failed_tests" = 0
        set __test_suites_success (math $__test_suites_success + 1)
    else
        set __test_suites_failed (math $__test_suites_failed + 1)
    end

    set -e __test_suite_active
    set -e __test_suite_failed_tests
    functions -q setUp; and functions -e setUp
    functions -q tearDown; and functions -e tearDown
    return 0
end