function assertFails
    if ! set -q __test_active
        echo "ERROR: Cannot make assertions outside from a test environment."
        echo "Type assertFails --help for more information"
        return 1
    end

    set -l expected_exit_status $argv[1]
    set -e argv[1]

    if test "$__active_test_failed" = true
        eval "$argv"
    else
        eval $argv > $__tmp_log_file 2>&1
        set -l exit_status $status
        if test "$exit_status" != "$expected_exit_status"
            echo >> $__tmp_log_file 
            echo "Expected exit status $expected_exit_status. Got $exit_status instead." >> $__tmp_log_file
            echo >> $__tmp_log_file            
            status -t >> $__tmp_log_file
            set __active_test_failed true
            return 1
        else
            test -f $__tmp_log_file; and rm $__tmp_log_file
        end
    end

    return 0
end