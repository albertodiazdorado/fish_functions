function assertEmpty
    if test "$argv" = "-h"; or test "$argv" = "--help"
        echo "Usage: assertEmpty [OPTIONS] ARGS"
        echo "Checks whether ARGS is empty."
        echo
        echo "assertEmpty can only be used within a fishTest. assertEmpty will fail if used"
        echo "outside of a fishTest block."
        echo "If ARGS is not empty, assertEmpty marks the active test as 'failed' and returns 1"
        echo
        echo "Options:"
        echo " -h, --help   Prints this help and exits."
        echo
        echo "Example:"
        echo "testSuite assertEmpty"
        return 0
    end

    if ! set -q __test_active
        echo "ERROR: Cannot make assertions outside from a test environment."
        echo "Type assertEmpty --help for more information"
        return 1
    end

    if test "$__active_test_failed" = false
        if count $argv > /dev/null
            echo -e "assertEmpty: Expected parameter to be empty, got $argv instead.\n" > $__tmp_log_file
            status -t >> $__tmp_log_file
            set __active_test_failed true
            return 1
        end
    end
end