function fishTest
    if status is-interactive
        echo "ERROR: Cannot use fishTest in an interactive terminal."
        echo "Type fishTest --help for more information."
        return 1
    end

    if ! set -q __test_suite_active
        status -t
        echo "ERROR: Cannot use fishTest outside of a testSuite."
        echo "Type fishTest --help for more information."
        return 1
    end

    functions -q setUp; and setUp
    set -g __test_active
    set -g __active_test_failed false
    set -g __test_name $argv

    if test -z "$argv"
        echo "ERROR: Cannot create a test without a name." > $__tmp_log_file 2>&1
        set -g __active_test_failed true
        set -g __test_name "--- MISSING TEST NAME ---" 
    end

    return 0
end