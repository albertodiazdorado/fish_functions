# Whatever happens here, is not really important
# Not even if the comment starts with testSuite
# testSuite

testSuite "Just some test suite name here"
	fishTest "A fishTest is only allowed within a fishTestSuite"
	endTest
endTestSuite

testSuite "You can have as many fishTestSuites as you want in a single test file"
	fishTest "However, every test suite must contain at least one fish test"
	endTest
endTestSuite
