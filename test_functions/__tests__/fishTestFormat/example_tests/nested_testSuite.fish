# In the future, we will allow nested testSuites
# However, this is not allowed for the time being

testSuite "The top-most test suite is completely fine"
    fishTest "A test here is ok"
    endTest

    testSuite "You can definitely not nest a testSuite"
    endTestSuite
endTestSuite