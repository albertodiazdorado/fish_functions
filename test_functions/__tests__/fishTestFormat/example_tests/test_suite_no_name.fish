testSuite "This is a well formed test suite"
    fishTest "This is a normal test"
    endTest
endTestSuite

testSuite
    fishTest "You will never execute this test, because the testSuite is ill-formed"
    endTest
endTestSuite