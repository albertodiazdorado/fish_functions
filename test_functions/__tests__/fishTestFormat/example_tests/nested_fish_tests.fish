testSuite "A normal test suite"
    fishTest "A normal test"
    endTest

    fishTest "A second test is completely fine!"
        fishTest "But you should not be nesting a test here..."
    endTest
endTestSuite