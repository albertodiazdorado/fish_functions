testSuite "Every test suite needs a closing tag"
    fishTest "You can have as many tests as you want"
    endTest
    
    fishTest "If you do not close the testSuite, you are doomed"
    endTest