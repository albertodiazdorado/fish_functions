testSuite "The test function fishTestFormat"
	function setUp
	    set -g testDir (pushd (dirname (status filename)); and pwd; and popd)/example_tests
	end
	
	function tearDown
	    set -q testDir
	    and set -e testDir
	end
	
	fishTest "checks the syntax of a well-formed fish test"
		assertSuccess fishTestFormat -n "$testDir/correct_test.fish"
	endTest
	
	fishTest "accepts fishTest's outside of a test suite"
		assertSuccess fishTestFormat -n "$testDir/no_test_suite.fish"
	endTest
	
	fishTest "fails if the testSuite's are not at the top-most level"
		assertSuccess test -f $testDir/testSuite_not_topMost.fish
		assertFails 1 fishTestFormat -n "$testDir/testSuite_not_topMost.fish"
	endTest
	
	fishTest "fails if the testSuite's are nested"
		assertSuccess test -f $testDir/nested_testSuite.fish
		assertFails 1 fishTestFormat -n "$testDir/nested_testSuite.fish"
	endTest
	
	fishTest "fails if the testSuite has no corresponding closing tag"
		assertSuccess test -f $testDir/no_closing_suite_tag.fish
		assertFails 1 fishTestFormat -n "$testDir/no_closing_suite_tag.fish"
	endTest
	
	fishTest "fails if endTestSuite has no corresponding opening tag"
		assertSuccess test -f $testDir/no_opening_suite_tag.fish
		assertFails 1 fishTestFormat -n "$testDir/no_opening_suite_tag.fish"
	endTest
	
	fishTest "fails if endTest has no corresponding opening tag"
		assertSuccess test -f $testDir/no_opening_test_tag.fish
		assertFails 1 fishTestFormat -n "$testDir/no_opening_test_tag.fish"
	endTest
	
	fishTest "fails if the fishTest has no closing tag"
		assertSuccess test -f $testDir/no_closing_test_tag.fish
		assertFails 1 fishTestFormat -n "$testDir/no_closing_test_tag.fish"
	endTest
	
	fishTest "fails if fishTest's are nested within each other"
		assertSuccess test -f $testDir/nested_fish_tests.fish
		assertFails 1 fishTestFormat -n "$testDir/nested_fish_tests.fish"
	endTest
	
	fishTest "fails if a testSuite has no name"
		assertSuccess test -f $testDir/test_suite_no_name.fish
		assertFails 1 fishTestFormat -n "$testDir/test_suite_no_name.fish"
	endTest
	
	fishTest "formats a well-formed test to STDOUT"
		set -l hashRegex '[\w\d]*'
		set -l expectedFormatedTest "$testDir/formatted_correct_test.fish"
		assertEqual (string match -r $hashRegex (sha256sum $expectedFormatedTest)) (string match -r $hashRegex (fishTestFormat "$testDir/correct_test.fish" | sha256sum))
	endTest
	
	fishTest "formats a well-formed test in-place"
		set -l hashRegex '[\w\d]*'
		set -l expectedFormatedTest "$testDir/formatted_correct_test.fish"
		set -l tempFile (mktemp)
		assertSuccess cp "$testDir/correct_test.fish" "$tempFile"
		assertSuccess fishTestFormat -i $tempFile
		assertEqual (string match -r $hashRegex (sha256sum $expectedFormatedTest)) (string match -r $hashRegex (sha256sum $tempFile))
	endTest
endTestSuite
