function assertEqual
    if ! set -q __test_active
        echo "ERROR: Cannot make assertions outside from a test environment."
        echo "Type assertEqual --help for more information"
        return 1
    end

    if test "$__active_test_failed" = true
        return 0
    end

    if test (count $argv) != 2
        echo "ERROR: Trying to call assertEqual with "(count $argv)" arguments." >> $__tmp_log_file
        status -t >> $__tmp_log_file
            set __active_test_failed true
        return 1
    end

    if test "$argv[1]" != "$argv[2]"
        echo "ERROR. Arguments are not equal." >> $__tmp_log_file
        echo "Expected: $argv[1]" >> $__tmp_log_file
        echo "Actual:   $argv[2]" >> $__tmp_log_file
        echo >> $__tmp_log_file
        status -t >> $__tmp_log_file
            set __active_test_failed true
        return 1
    end

    return 0
end