function fishTestFormat
    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s n -l dry-run)
    set -l options $options (fish_opt -s i -l in-place)
    set -l options $options (fish_opt -s o -l output -r)

    argparse --name "fishFormat" $options -- $argv
    or return 127

    if set -q _flag_h
        _print_help
        return
    end

    if test (count $argv) != 1
        echo "Expected exactly 1 file. "(count $argv)" provided instead."
        return 1
    end

    if ! test -f $argv
        echo "File $argv does not exist."
        return 1
    end

    set -l STACK
    set -l LINE_COUNT 0
    set -l RETURN_VALUE 0

    fish_indent -i $argv | while read -l line
        set LINE_COUNT (math $LINE_COUNT + 1)

        if test $RETURN_VALUE = 1
            return 1
        end
        
        switch (string match -r '^[\w]*' $line)
            case "testSuite"
                if test (count $STACK) = 0
                    set testName (string match -r '^testSuite\s*(.*)' "$line")[2]
                    if test (string length $testName) = 0
                        echo "Syntax error: testSuite is missing a name at line $LINE_COUNT."
                        set RETURN_VALUE 1
                        break
                    end

                    set STACK $STACK "testSuite"
                else
                    echo "Syntax error: testSuite is not at the top-most level, at line $LINE_COUNT."
                    echo "$STACK[-1] was called before."
                    set RETURN_VALUE 1
                    break
                end
            case "endTestSuite"
                if test "$STACK[-1]" = "testSuite"
                    set -e STACK[-1]
                else if test (count $STACK) = 0
                    echo "Syntax error: endTestSuite bad usage at line $LINE_COUNT."
                    echo "No matching testSuite tag found."
                    set RETURN_VALUE 1
                    break
                else
                    echo "Syntax error: endTestSuite bad usage at line $LINE_COUNT."
                    echo "endTestSuite called before $STACK[-1] was closed."
                    set RETURN_VALUE 1
                    break
                end
            case "fishTest"
                set STACK $STACK "fishTest"
            case "endTest"
                if test "$STACK[-1]" = "fishTest"
                    set -e STACK[-1]
                else
                    echo "Syntax error: endTest bad usage at line $LINE_COUNT."
                    echo "No matching fishTest tag found."
                    set RETURN_VALUE 1
                    break
                end
        end
    end

    if test $RETURN_VALUE = 1
        return 1
    end

    if test (count $STACK) != 0
        echo "Syntax error: Missing closing tag for $STACK[-1]"
        return 1
    end

    if set -q _flag_n
        return
    end

    set -l FILE_DESCRIPTOR (mktemp)
    set -l INDENT ""
    
    tabs -4 > /dev/null
    fish_indent $argv | while read -l line
        switch (string match -r '^[\w]*' (string trim $line))
            case "testSuite" or "fishTest"
                echo -e $INDENT$line >> $FILE_DESCRIPTOR
                set INDENT "$INDENT\t"
            case "endTestSuite" or "endTest"
                set INDENT (string sub --length (math (string length $INDENT) - 2) $INDENT)
                echo -e $INDENT$line >> $FILE_DESCRIPTOR
            case '*'
                echo -e $INDENT$line >> $FILE_DESCRIPTOR
        end
    end

    if set -q _flag_i
        cp $FILE_DESCRIPTOR $argv
    else if set -q _flag_o
        cp $FILE_DESCRIPTOR $_flag_o
    else
        cat $FILE_DESCRIPTOR
    end

    test -f $FILE_DESCRIPTOR; and rm $FILE_DESCRIPTOR
end

function _print_help
    echo "USAGE: fishFormat [OPTIONS] FILE"
    echo "Checks the syntax of fishTestSuites and fishTests."
    echo "Formats FILE by indenting fishTestSuites and testSuites, and prints the result to STDOUT."
    echo
    echo "OPTIONS:"
    echo "  -n, --dry-run     Only do syntax checking."
    echo "  -i, --in-place    Overwrite the input files, instead of printing the result to STDOUT."
    echo "  -o, --output FILE Write the formatted test to FILE."
end