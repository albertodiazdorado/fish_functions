function runTests
    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s e -l exclude -r)
    argparse $options -- $argv

    if set -q _flag_h
        __print_help
        return 0
    end
    set -l excluded_tests
    if set -q _flag_e
        set excluded_tests $_flag_e
    end
    set -l parent_dir $argv

    set -g __test_suites_success 0
    set -g __test_suites_failed 0
    set -g __test_success 0
    set -g __test_failed 0
    set -g __tmp_log_file (mktemp)

    set -l tic (math (date +%s%N)/1000000000)
    for fish_test in (find $parent_dir -type f -name "*.test.fish")
        set -l TEST_NAME (string match --regex "[^/]*\.test\.fish" $fish_test)
        set TEST_NAME (string match --regex "\w+" $TEST_NAME)
        if test $excluded_tests
            contains $TEST_NAME $excluded_tests; and continue
        end
        fishTestFormat -n "$fish_test" > $__tmp_log_file
        if test $status = 1
            echo -e "\n\033[1;31m"\u2718"\033[0;1m Error while parsing $fish_test\033[0m"
            test -f $__tmp_log_file; and awk '{printf "  %s\n", $0}' < $__tmp_log_file
            test -f $__tmp_log_file; and rm $__tmp_log_file
            set __test_suites_failed (math $__test_suites_failed + 1)            
        else
            source $fish_test
        end
    end
    set -l toc (math (date +%s%N)/1000000000)
    set -l elapsed_time (math -s3 $toc-$tic)

    __print_summary $elapsed_time

    set -l returnValue $__test_suites_failed
    
    set -e __test_suites_success
    set -e __test_suites_failed
    set -e __test_success
    set -e __test_failed
    test -f $__tmp_log_file; and rm $__tmp_log_file
    set -e __tmp_log_file

    return $returnValue
end

function __print_help
    echo "Usage: runTests [BASEPATH] [OPTIONS]"
    echo "Find and executes all the fish tests in BASEPATH."
    echo 
    echo "If the BASEPATH is left empty, the current directory will be used as BASEPATH."
    echo "Options:"
    echo "  -h, --help          Display this help and exits"
    echo "  -e, --exclude TEST  Excludes TEST, where TEST is 'mytest' and not 'mytest.test.fish'"
end

function __print_summary
    set -l BOLD "\033[1m"
    set -l NORMAL "\033[0m"
    set -l RED "\033[1;31m"
    set -l GREEN "\033[1;32m"
    set -l YELLOW "\033[1;33m"

    set -l test_suites_report $BOLD"Test Suites:  $NORMAL"
    if test "$__test_suites_failed" != 0
        set test_suites_report $test_suites_report "$RED$__test_suites_failed failed$NORMAL,"
    end
    if test "$__test_suites_success" != 0
        set test_suites_report $test_suites_report $GREEN"$__test_suites_success passed$NORMAL,"
    else
        set test_suites_report $test_suites_report "$__test_suites_success passed,"
    end
    set test_suites_report $test_suites_report (math $__test_suites_success + $__test_suites_failed) "total."
    
    set -l test_report $BOLD"Tests:        $NORMAL"
    if test "$__test_failed" != 0
        set test_report $test_report "$RED$__test_failed failed$NORMAL,"
    end
    if test "$__test_success" != 0
        set test_report $test_report "$GREEN$__test_success passed$NORMAL,"
    else
        set test_report $test_report "$__test_success passed,"
    end
    set test_report $test_report (math $__test_success + $__test_failed) "total."

    echo -e "\n----------------------"
    echo -e $test_suites_report
    echo -e $test_report
    echo -e $BOLD"Time:         "$YELLOW $argv"s"
    echo -e $NORMAL"\nRan all test suites."
end