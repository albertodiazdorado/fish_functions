set -l ABS_PATH (dirname (status filename))

source $ABS_PATH'/runTests.fish'
source $ABS_PATH'/testSuite.fish'
source $ABS_PATH'/endTestSuite.fish'
source $ABS_PATH'/fishTest.fish'
source $ABS_PATH'/endTest.fish'
source $ABS_PATH'/assertSuccess.fish'
source $ABS_PATH'/assertFails.fish'
source $ABS_PATH'/assertEqual.fish'
source $ABS_PATH'/assertEmpty.fish'
source $ABS_PATH'/fishTestFormat.fish'