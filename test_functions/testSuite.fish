function testSuite
    if status is-interactive
        echo "ERROR: Cannot use fishTest in an interactive terminal."
        echo "Type fishTest --help for more information."
        return 1
    end
    
    if set -q __test_suite_active
        status -t
        echo "ERROR: Cannot create a test suite within an existing test suite."
        echo "End the previous test suite before creating a new one."
        echo -e "Type \033[1mtestSuite --help\033[0m for more information."
        return 1
    end
    set -g __test_suite_active
    set -g __test_suite_failed_tests 0
    functions -q setUp; and functions -e setUp
    functions -q tearDown; and functions -e tearDown

    echo -e "\n\033[1m$argv...\033[0m"
    
    return 0
end