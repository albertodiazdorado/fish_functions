function assertSuccess
    if ! set -q __test_active
        echo "ERROR: Cannot make assertions outside from a test environment."
        echo "Type assertSuccess --help for more information"
        return 1
    end

    if ! __assert_commands_exist "$argv"
        return 1
    end

    if test "$__active_test_failed" = true
        eval $argv > /dev/null 2>&1
    else
        eval $argv > $__tmp_log_file 2>&1
        set -l exit_status $status
        if test "$exit_status" != 0
            echo >> $__tmp_log_file
            status -t >> $__tmp_log_file
            set __active_test_failed true
            return 1
        else
            test -f $__tmp_log_file; and rm $__tmp_log_file
        end
    end

    return 0
end

function __assert_commands_exist
    set -l piped_args (echo "$argv" | string split "|" | string trim)
    for piped_arg in $piped_args
        set -l cmd (echo $piped_arg | string split " ")
        if ! type -q $cmd[1]
            echo -e "assertSucces: command $cmd[1] does not exist\n" > $__tmp_log_file
            status -t >> $__tmp_log_file
            set __active_test_failed true
            return 1
        end
    end
end