function endTest
    if status is-interactive
        echo "ERROR: Cannot use fishTest in an interactive terminal."
        echo "Type fishTest --help for more information."
        return 1
    end
    
    if ! set -q __test_active
        status -t
        echo -e "ERROR: Using \033[1mendTest\033[0m outside of a test."
        echo -e "Cannot use \033[1mfishTest\033[0m without using \033[1mendTest\033[0m first."
        echo "Type endTest --help for more information."
        return 1
    end

    set -l CHECKMARK "\033[1;32m"\u2714"\033[0m"
    set -l FAILED "\033[1;31m"\u2718"\033[0m"
    set -l TEST_MARK
    set -l returnValue
    if test "$__active_test_failed" = "true"
        set TEST_MARK $FAILED
        set -g __test_failed (math $__test_failed + 1)
        set -g __test_suite_failed_tests (math $__test_suite_failed_tests + 1)
        set returnValue 1
    else
        set TEST_MARK $CHECKMARK
        set -g __test_success (math $__test_success + 1)
        set returnValue 0
    end

    echo -e "  $TEST_MARK  \033[1m$__test_name\033[0m"
    test -f $__tmp_log_file; and awk '{printf "      %s\n", $0}' < $__tmp_log_file
    test -f $__tmp_log_file; and rm $__tmp_log_file
    set -e __test_active
    set -e __active_test_failed
    set -e __test_name
    functions -q tearDown; and tearDown
    
    return $returnValue
end