# Custom fish functions

My utility functions for the fish shell.
Includes a testing framework for the fish shell :sunglasses:.

## Run all tests
`fish run_ci.fish`

## To-do's

* [x] Write a parser that checks that test suites and fish tests have proper opening and closing tags. Use in runTests
* [x] Proper error message if a test suite or fish test lacks a name. Also log the suite/test as failed. Use in runTests
* [ ] Add MOCKS
* [ ] Incorporate the testing framework to the fish-functions
* [ ] Nested test suites
* [ ] Parameterized tests
* [ ] Allow to use assertions outside of tests
* [x] Do not allow to use fishTest's and testSuite's interactively
* [x] Add a formatter that indents the fishTests after fish_indent is executed. Probably merge it with the opening and closing tags checker
* [ ] Allow to skip tests and test suites
