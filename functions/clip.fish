function clip
    set -l ERROR "\033[1;31mERROR:\033[0m"
    if ! type -q xset
        echo -e "$ERROR The command xset is not defined. Cannot use clip"
        return 1
    end

    if ! xset q >/dev/null
        echo -e "$ERROR No X server configured. Cannot use clip"
        return 1
    end

    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s s -l silent)

    if ! argparse -s $options -- $argv
        return 1
    end

    if set -q _flag_h
        _print_help
        return
    end

    $argv | xclip -r -selection c
    if ! set -q _flag_s
        xclip -o -selection c
    end
end

function _print_help
    echo "Usage: clip [OPTIONS] COMMAND"
    echo
    echo "Executes the COMMAND and copies the stdout to the clipboard."
    echo "Literally identical to COMMAND | xclip -r -selection c."
    echo "Options:"
    echo "  -h, --help     Prints this help and exits"
    echo "  -s, --silent   Do not print anything to stdout"
    echo
    echo "Example:"
    echo "  clip mkdir -v MyFolder"
    echo "  Content of the clipboard: \"mkdir: created directory 'MyFolder'\""
end