set ERROR "\033[1;31mERROR:\033[0m"
set WARNING "\033[1;33mWARNING:\033[0m"
set INFO "\033[1mINFO:\033[0m"
set CONFIG_INFO "\033[1;36mCONFIGURATION INFO:\033[0m"

function update-aur
    set -l USER_FOLDER $HOME
    set -l DEFAULT_AUR_FOLDER $USER_FOLDER/build
    set -l AUR_FOLDER
    set -l yes
    set -l custom_config
    set -l forced false
    set -l EXCLUDED_FOLDERS "oh-my-fish"
    set -l pass

    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s u -l user -r)
    set -l options $options (fish_opt -s y -l yes)
    set -l options $options (fish_opt -s a -l aur -r)
    set -l options $options (fish_opt -s f -l force)
    set -l options $options (fish_opt -s e -l exclude -r)
    set -l options $options (fish_opt -s c -l config -o)
    set -l options $options (fish_opt -s p -l password -o)

    # Handle arguments
    argparse $options -- $argv

    ## Print help
    if set -q _flag_h
        _print_help
        return
    end

    ## Define user
    if set -q _flag_u
        echo -e "$CONFIG_INFO Updating fish functions of user \"$_flag_u\"..."
        set USER_FOLDER (getent passwd $_flag_u | cut -d: -f6)
        if test ! -d "$USER_FOLDER"
            echo -e "$ERROR There is no home folder for user \"$_flag_u\". Aborting..." | log
            return 1
        end
        set DEFAULT_AUR_FOLDER $USER_FOLDER/build
    end

    ## Answer yes to everything
    if set -q _flag_y
        set yes "--noconfirm"
    end

    ## Set the folder where you wanna install your packages
    if set -q _flag_a
        echo -e "$CONFIG_INFO Using $USER_FOLDER/$_flag_a as the AUR build repository..."
        set AUR_FOLDER $USER_FOLDER/$_flag_a
    end

    ## Exclude folders
    if set -q _flag_e
        echo -e "$WARNING Excluding folder(s) $_flag_e" | log
        set -a EXCLUDED_FOLDERS $_flag_e
    end

    ## Force re-installation
    if set -q _flag_f
        echo -e "$CONFIG_INFO Option FORCED was chosen. All packages will be reinstalled." | log
        set forced true
    end

    ## Get password
    if set -q _flag_p
        if test $_flag_p
            set pass $_flag_p
        else
            echo "Enter your password to automate all operations that require it"
            read -P "Password: " pass
        end
        set yes "--noconfirm"
    end

    ## Specify .makepkg.conf
    if set -q _flag_c
        if test $_flag_c
            if test -f $_flag_c
                echo -e "$CONFIG_INFO Using custom config file $_flag_c"
                set custom_config "$_flag_c"
            else
                echo -e "$CONFIG_INFO Custom config file $_flag_c not found. Using default configuration." | log
            end
        else
            if test -f $USER_FOLDER/.makepkg.conf
                echo -e "$CONFIG_INFO Using custom config file at $USER_FOLDER/.makepkg.conf"
                set custom_config "$USER_FOLDER/.makepkg.conf"
            else
                echo -e "$CONFIG_INFO Custom config file $USER_FOLDER/.makepkg.conf not found. Using default configuration." | log
            end
        end
    end

    if ! test $AUR_FOLDER
        echo -e "$CONFIG_INFO No AUR folder provided. Using $DEFAULT_AUR_FOLDER instead."
        set AUR_FOLDER $DEFAULT_AUR_FOLDER
    end

    if ! test -d $AUR_FOLDER
        _create_AUR_directory $AUR_FOLDER $yes
        if test $status != 0
            return $status
        end
    end

    for package in (find $AUR_FOLDER -maxdepth 1 -type d | sort -n)
        if test $package = $AUR_FOLDER
            continue
        end
        echo -e "\n\n$INFO Entering directory $package..."
        echo -e "\033[1m---------------------------\033[0m"

        if contains $package $AUR_FOLDER/$EXCLUDED_FOLDERS
            echo -e "$WARNING Skipping excluded package $package..."
            continue
        end

        pushd $package

        git config pull.rebase true
        git fetch --all
        set git_pull_result (git pull)

        if test $status != 0
            echo -e "$ERROR Command 'git pull' failed for package $package." | log
            echo -e "$INFO Continuing to the next folder..."
            popd
            continue
        end

        if test "$git_pull_result" = "Already up to date." -a "$forced" != true
            echo -e "$INFO Already up to date. Skipping..."
        else
            if test $pass
                echo -e "$INFO Making package $package and installing all dependencies manually..."
                _manual_install "$pass" "$custom_config_flag"
            else
                echo -e "$INFO Making package $package..."
                if test "$custom_config"
                    makepkg -sric --config "$custom_config" "$yes"
                    if test $status != 0
                        echo -e "$ERROR Command `makepkg -sric --config $custom_config $yes` failed for package $package."
                        echo -e "$INFO Continuing to the next folder..."
                    end
                else
                    makepkg -sric "$yes"
                    if test $status != 0
                        echo -e "$ERROR Command `makepkg -sric $yes` failed for package $package."
                        echo -e "$INFO Continuing to the next folder..."
                    end
                end
            end

            git clean -fdx
        end

        popd
    end

    return 0
end

function _manual_install
    set -l pass $argv[1]
    set -l custom_config $argv[2]

    _install_dependencies $pass
    _install_make_dependencies $pass
    _install_opt_dependencies $pass

    if test $custom_config
        makepkg -rc --config "$custom_config"
        if test $status != 0
            echo -e "$ERROR Command `makepkg -rc --config $custom_config` failed to build $package."
            echo -e "$INFO Continuing to the next folder..."
        end
    else
        makepkg -rc
        if test $status != 0
            echo -e "$ERROR Command `makepkg -rc` failed to build $package."
            echo -e "$INFO Continuing to the next folder..."
        end
    end

    echo "$pass" | sudo -S pacman -U (ls | grep pkg.tar) --noconfirm
end

function _install_dependencies
    set -l pass $argv[1]
    for dep in (makepkg --printsrcinfo PKGBUILD | grep -P '\tdepends' | sed 's/\tdepends = \(.*\)/\1/')
        echo "$pass" | sudo -S pacman -S $dep --noconfirm
    end
end

function _install_make_dependencies
    set -l pass $argv[1]
    for dep in (makepkg --printsrcinfo PKGBUILD | grep -P '\tmakedepends' | sed 's/\tmakedepends = \(.*\)/\1/')
        echo "$pass" | sudo -S pacman -S $dep --noconfirm
    end
end

function _install_opt_dependencies
    set -l pass $argv[1]
    for dep in (makepkg --printsrcinfo PKGBUILD | grep -P '\toptdepends' | sed 's/\toptdepends = \(.[^:]*\).*/\1/')
        echo "$pass" | sudo -S pacman -S $dep --noconfirm
    end
end

function _create_AUR_directory
    set -l confirm
    set -l AUR_DIR $argv[1]
    set -l noconfirm $argv[2]

    echo -e "$WARNING The directory $AUR_DIR does not exist."
    if test $noconfirm
        set confirm y
    else
        read -P "Do you want to create it? [y/N]" confirm
    end

    switch $confirm
        case y Y
            echo -e "$INFO Creating folder $AUR_DIR..."
            mkdir $AUR_DIR
            set -l mkdirStatus $status
            if test $mkdirStatus != 0
                echo -e "$ERROR Failed to create directory $AUR_DIR with error code $mkdirStatus." | log
                return 1
            end
            return
        case '*'
            echo -e "$ERROR Directory $AUR_DIR does not exist. Aborting..." | log
            return 1
    end
end

function _print_help
    echo "Usage: update-aur [OPTIONS]"
    echo "Update all manually-installed ARCH packages."
    echo -e "\nGo through a list of packages from the AUR repository. Check whether any of them "
    echo "has received any updates. If so, install thenewest version. Optionally exclude or "
    echo "force the re-installation of certain packages."
    echo -e "\nMandatory arguments to long options are mandatory for short options too."
    echo "  -h, --help              Display this help and exits"
    echo "  -u, --user=USER         Update the AUR packages of a certain user. It will fail if the user"
    echo "                          does not have a HOME folder, or if you do not have rights to manage"
    echo "                          that user account. The default is the current user."
    echo "  -y, --yes               Do not ask for any confirmation. Answer yes to any prompt"
    echo "  -a, --aur=DIR           Specify the directory where the AUR packages live, as they have been"
    echo "                          cloned from the AUR repository. The directory is relative to the "
    echo "                          HOME folder of the user (this can be modified by the --user flag)."
    echo "                          Defaults to HOME/build if not provided."
    echo "  -f, --force             Force the re-install of all packages, even if there are no updates."
    echo "  -e, --exclude=PKGS      Exclude packages."
    echo "  -c, --config=[CONF]     Provide a custom makepkg.conf file to the makepkg command. If the "
    echo "                          flag --config is provided with no argument, it will be load the   "
    echo "                          file at HOME/.makepkg.conf (if it exists)."
    echo "  -p, --password=[PASS]   Trigger manual installation of the packages, using pacman instead of"
    echo "                          makepkg. Either pass the password as an argument, or enter the"
    echo "                          password when the script asks for it. Manual installation has the"
    echo "                          advantage of installing all dependencies (including make"
    echo "                          dependencies and optional dependencies) if they are missing, which"
    echo "                          makepkg does not do."
    echo "END"
end

function log
    if isatty
        fold -s -w (tput cols) <&0
    else
        cat <&0
    end
end