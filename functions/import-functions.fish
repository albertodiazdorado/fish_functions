set ERROR "\033[1;31mERROR:\033[0m"
set WARNING "\033[1;33mWARNING:\033[0m"
set INFO "\033[1mINFO:\033[0m"

function import-functions
    set -l override false
    set -l clean false
    set -l remote "https://gitlab.com/albertodiazdorado/fish_functions.git"
    set -l FISH_FOLDER $HOME/.config/fish/functions

    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s o -l override)
    set -l options $options (fish_opt -s c -l clean)

    argparse --name "import-functions" $options -- $argv
    or return 1

    if set -q _flag_h
        _print_help
        return
    end

    if ! test -d $FISH_FOLDER
        _create_directory "$FISH_FOLDER"
        if test $status != 0
            return $status
        end
    end

    # Create a random name for the temporary folder
    set -l temp_folder (cat /proc/sys/kernel/random/uuid)
    echo -e "$INFO Cloning functions into temp folder $temp_folder..."
    git clone $remote "$HOME/$temp_folder" 2>/dev/null

    if set -q _flag_c
        echo -e "$WARNING Cleaning all content of $FISH_FOLDER before installation"
        if count (ls -A $FISH_FOLDER)
            rm -r $FISH_FOLDER/*
        end
    end

    set -l return_value
    if set -q _flag_o
        echo -e "$WARNING Overriding existing functions in $FISH_FOLDER"
        echo -e "$INFO Copying functions into $FISH_FOLDER..."
        cp -f $HOME/$temp_folder/functions/* $FISH_FOLDER
        set return_value $status
    else
        echo -e "$INFO Copying functions into $FISH_FOLDER..."
        cp -n $HOME/$temp_folder/functions/* $FISH_FOLDER
        set return_value $status
    end

    echo -e "$INFO Removing temp folder $temp_folder..."
    rm -rf "$HOME/$temp_folder"

    return $return_value
end

function _create_directory
    set -l DIR "$argv[1]"
    echo -e "$WARNING Directory $DIR does not exist."
    read -l -P "Do you want to create it? [y/N]" confirm

    switch $confirm
        case y Y
            echo -e "$INFO Creating folder $DIR..."
            mkdir $DIR
            if test $status != 0
                echo -e "$ERROR Failed to create directory $DIR. Aborting..." | _log
                return 1
            end
        case '*'
            echo -e "$ERROR Directory $DIR does not exist. Aborting..." | _log
            return 1
    end
end

function _print_help
    echo "USAGE: import-functions [OPTIONS]"
    echo "* Installs custom fish functions from the repository $remote"
    echo "* Functions will be installed under XDG_CONFIG_HOME/fish/functions by default. If XDG_CONFIG_HOME is not defined, functions will be installed under HOME/.config/fish/functions" | _log
    echo -e "\nOPTIONS:"
    echo "  -o, --override  Overwrites existing functions in the installation folder."
    echo "                  The default value is false (preserve local functions in case of name conflict)" | _log
    echo "  -c, --clean     Remove all local functions before installation (clean install)" | _log
    echo "                  The default value is false (preserve local functions)"
    echo "  -h, --help      Print this help and exits"
end

function _log
    if isatty
        fold -s -w (tput cols) <&0
    else
        cat <&0
    end
end