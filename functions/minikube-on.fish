function minikube-on
    if ! type -q minikube
        set -l ERROR "\033[1;31mERROR:\033[0m"
        echo -e "$ERROR minikube is not installed. Aborting..."
        return 1
    end

    set -l LEASES_FILE "$HOME/.config/VirtualBox/HostInterfaceNetworking-vboxnet0-Dhcpd.leases"
    if test -f $LEASES_FILE
        echo "MINIKUBE-ON: Deleting file $LEASES_FILE..."
        rm $LEASES_FILE
    end

    # set -l LEASES_FILE "$HOME/.config/VirtualBox/HostInterfaceNetworking-vboxnet0-Dhcpd.leases-prev"
    # if test -f $LEASES_FILE
    #     echo "MINIKUBE-ON: Deleting file $LEASES_FILE..."
    #     rm $LEASES_FILE
    # end

    minikube start
end