set ERROR "\033[1;31mERROR:\033[0m"
set WARNING "\033[1;33mWARNING:\033[0m"
set INFO "\033[1mINFO:\033[0m"

function init-latex
    set -l CONFIG_DIR (pushd (dirname (status --current-filename)); and pwd; and popd)
    set -l DIR (pwd)
    set -l name "main"
    set -l init_latex_git false
    set -l init_latex_gi false

    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s c -l cheatsheet -o)
    set -l options $options (fish_opt -s g -l git)
    set -l options $options (fish_opt -s n -l name -r)
    set -l options $options (fish_opt -s d -l dir -r)

    argparse $options -- $argv

    if set -q _flag_h
        _print_help | log
        return
    end
    if set -q _flag_c
        _open_cheatsheet $CONFIG_DIR $_flag_c
        return
    end
    if set -q _flag_g
        _check_git_availability
        set -l git_availability $status

        switch $git_availability
            case 0
                set init_latex_git true
                set init_latex_gi true
            case 1
                set init_latex_git false
                set init_latex_gi false
            case 2
                set init_latex_git true
                set init_latex_gi false
        end
    end
    if set -q _flag_n
        set name $_flag_n
    end
    if set -q _flag_d
        if test ! -d $_flag_d
            _create_dir $_flag_d
            if test $status != 0
                return $status
            end
        end
        set DIR $_flag_d
    end

    if ! test -f $CONFIG_DIR/template.tex
        echo -e "$ERROR File template.tex not found. Aborting..." | log
        return 1
    end

    echo -e "$INFO Creating template file $name.tex at $DIR..." | log
    cp $CONFIG_DIR/template.tex $DIR
    chmod +w $DIR/template.tex
    mv $DIR/template.tex $DIR/$name.tex

    if test $init_latex_git = true
        echo -e "$INFO Initializing git repository..." | log
        pushd $DIR
        git init
        popd
    end

    if test $init_latex_gi = true
        echo -e "$INFO Appending temporary latex files to .gitignore..." | log
        gi latex >>$DIR/.gitignore
    end
end

function _print_help
    echo "Usage: init-latex [OPTIONS [ARGS]]"
    echo "Creates a basic latex template on the current directory."
    echo -e "* Use \033[1mlatexmk -pdf\033[0m to compile the template."
    echo -e "* Use \033[1mlatexmk -pdf -pvc\033[0m to compile the template in watch mode."
    echo -e "* Use \033[1mlatexindent.pl -w [FILE]\033[0m to format the file in-place."
    echo -e "\nOPTIONS:"
    echo "  -d, --dir         Specify the directory where the template is created."
    echo "  -n, --name        Give a name to the template. Default is 'main'."
    echo "  -g, --git         Create a git repository."
    echo "                    Also create a gitignore file for temporary latex documents."
    echo "  -c, --cheatsheet  Open a cheatsheet of basic latex commands."
    echo "                    * If init-latex fails to find the cheatsheet locally,"
    echo "                    init-latex will try to download it from source."
    echo "                    * Use the argument 'browser' to open the source file."
    echo "                    * The default pdf reader is used otherwise."
    echo "  -h, --help        Prints this help and exits."
end

function _create_dir
    set -l DIR $argv
    echo -e "$WARNING Directory $DIR does not exist."
    read -l -P "Do you want to create it? [y/N]" confirm

    switch $confirm
        case y Y
            echo -e "$INFO Creating folder $DIR..."
            mkdir $DIR
            if test $status != 0
                echo -e "$ERROR Failed to create directory $DIR. Aborting..." | log
                return 1
            end
        case '*'
            echo -e "$ERROR Directory $DIR does not exist. Aborting..." | log
            return 1
    end

    return 0
end

function _open_cheatsheet
    set -l cheatsheet_address "https://wch.github.io/latexsheet/latexsheet-a4.pdf"
    set -l DIR $argv[1]
    set -l remote $argv[2]

    if test "$remote" = "browser"
        echo -e "$INFO Opening latex cheatsheet in the broswer..." | log
        open $cheatsheet_address
        return
    end

    if ! test -f $DIR/cheatsheet.pdf
        echo -e "$WARNING cheatsheet not found. Trying to download..." | log
        if ! curl --head -s $cheatsheet_address | head -n 1 | grep 200 >/dev/null
            echo -e "$ERROR Remote resource could not be fetched. Aborting..." | log
            return 1
        end

        curl -sL $cheatsheet_address --output $DIR/cheatsheet.pdf

        if test $status != 0
            echo -e "$ERROR Error while downloading remote data. Aborting..." | log
            return 1
        end
    end

    echo -e "$INFO Opening latex cheatsheet..." | log
    open $DIR/cheatsheet.pdf
end

function _check_git_availability
    if ! git --help >/dev/null
        echo -e "$WARNING Function git is not installed." | log
        echo -e "$WARNING Will not create a git repository." | log
        return 1
    end

    if ! functions gi >/dev/null
        echo -e "$WARNING Function gi is missing." | log
        echo -e "$WARNING Creating latex template without .gitignore file." | log
        return 2
    end

    if ! _check_internet_connectivity
        echo -e "$WARNING There is no internet connectivity." | log
        echo -e "$WARNING Creating latex template without .gitignore file." | log
        return 2
    end

    return
end

function log
    if isatty
        fold -s -w (tput cols) <&0
    else
        cat <&0
    end
end

function _check_internet_connectivity
    return (nc -zw1 google.com 443)
end