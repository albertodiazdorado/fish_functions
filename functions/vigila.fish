function vigila
    set -l options (fish_opt -s h -l help)
    set -l options $options (fish_opt -s i -l interval -r)

    if ! argparse -s $options -- $argv
        return 1
    end

    if set -q _flag_h
        _print_help
        return
    end

    set -l comando $argv
    set -l intervalo 2.0
    if set -q _flag_i
        set intervalo $_flag_i
    end

    while sleep $intervalo
        eval $comando >/tmp/vigila_file
        clear
        echo -e "Every $intervalo seconds: $argv\n"
        cat /tmp/vigila_file
    end
end

function _print_help
    echo "Usage: vigila [OPTIONS] COMMAND"
    echo
    echo "Similar to watch, but prints color to the terminal and understands"
    echo "aliases and custom functions. Executes the given COMMAND every few"
    echo "seconds."
    echo
    echo "Options:"
    echo "  -h, --help              Prints this help and exits"
    echo "  -i, --interval <secs>   Interval between the execution of commands."
    echo "                          Default is 2 seconds."
end