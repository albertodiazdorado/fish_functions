function gi
    set -l options (fish_opt -s h -l help)

    if ! argparse -s $options -- $argv
        return 1
    end

    if set -q _flag_h
        _print_help
        return
    end

    curl -L -s https://www.gitignore.io/api/$argv
end

function _print_help
    echo "Usage: gi [OPTIONS] TYPE"
    echo
    echo "Call the gitignore.io api for the given TYPE, which"
    echo "prints the suggested .gitignore file to STDOUT."
    echo "Options:"
    echo "  -h, --help     Prints this help and exits"
    echo "Example:"
    echo "  gi latex >> .gitignore"
end