testSuite "The update-aur helpers for manuall package installation"
	
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    functions -q update-aur && functions -e update-aur
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/update-aur.fish"
	
	    set -g build_dir "$HOME/build"
	    set -g sudo_log "$HOME/sudo_log.txt"
	    set -g makepkg_log "$HOME/makepkg_log.txt"
	end
	
	function tearDown
	    set HOME $OLD_HOME
	    set -q OLD_HOME && set -e OLD_HOME
	    test -f $sudo_log && rm $sudo_log
	    test -f $makepkg_log && rm $makepkg_log
	end
	
	fishTest "install dependencies"
		# given
		function sudo
		    echo $argv >>$sudo_log
		end
		
		function makepkg
		    echo $argv >>$makepkg_log
		    cat "$HOME/SRCINFO/SRCINFO-google-chrome"
		end
		
		# when
		assertSuccess _install_dependencies "secret"
		
		# then
		set -l makepkg_calls (cat $makepkg_log)
		assertEqual "$makepkg_calls" "--printsrcinfo PKGBUILD"
		
		set expected_dependencies "alsa-lib" "gtk3" "libcups" "libxss" "libxtst" "nss"
		set -l index 1
		assertSuccess test -f $sudo_log
		while read line
		    assertEqual "-S pacman -S $expected_dependencies[$index] --noconfirm" "$line"
		    set index (math $index+1)
		end <$sudo_log
		
		# tearDown
		functions -q sudo && functions -e sudo
		functions -q makepkg && functions -e makepkg
	endTest
	
	fishTest "install optional dependencies"
		# given
		function sudo
		    echo $argv >>$sudo_log
		end
		
		function makepkg
		    echo $argv >>$makepkg_log
		    cat "$HOME/SRCINFO/SRCINFO-google-chrome"
		end
		
		# when
		assertSuccess _install_opt_dependencies "secret"
		
		# then
		set expected_dependencies "libpipewire02" "kdialog" "gnome-keyring" "kwallet" "gtk3-print-backends" "libunity" "ttf-liberation" "xdg-utils"
		set -l index 1
		assertSuccess test -f $sudo_log
		while read line
		    assertEqual "-S pacman -S $expected_dependencies[$index] --noconfirm" "$line"
		    set index (math $index+1)
		end <$sudo_log
		
		# tearDown
		functions -q sudo && functions -e sudo
	endTest
	
	fishTest "get password from command line, then install packages manually"
		# given
		set -l BUILD "$HOME/build"
		mkdir "$BUILD"
		mkdir "$BUILD/package1"
		touch "$BUILD/package1/package1.pkg.tar.xz"
		set -l testPass "myPassword"
		set -g install_log "$HOME/install_log.txt"
		
		function sudo
		    echo $argv >>$sudo_log
		end
		
		function makepkg
		    echo "$argv" >>$makepkg_log
		    return 0
		end
		
		function git
		    return 0
		end
		
		function _install_dependencies
		    echo "$argv" >>$install_log
		end
		function _install_make_dependencies
		    echo "$argv" >>$install_log
		end
		function _install_opt_dependencies
		    echo "$argv" >>$install_log
		end
		
		# when
		assertSuccess update-aur --password="$testPass"
		
		# then
		set -l sudo_log_content (cat $sudo_log)
		assertEqual "$sudo_log_content" "-S pacman -U package1.pkg.tar.xz --noconfirm"
		
		set -l makepkg_log_content (cat $makepkg_log)
		assertEqual "$makepkg_log_content" "-rc"
		
		assertSuccess test -f $install_log
		while read line
		    assertEqual "$line" "$testPass"
		end <$install_log
		
		# tearDown
		functions -q sudo && functions -e sudo
		functions -q makepkg && functions -e makepkg
		functions -q git && functions -e git
		functions -q _install_dependencies && functions -e _install_dependencies
		functions -q _install_make_dependencies && functions -e _install_make_dependencies
		functions -q _install_opt_dependencies && functions -e _install_opt_dependencies
		test -d "$BUILD" && rm -rf "$BUILD"
		test -f "$install_log" && rm "$install_log"
	endTest
	
endTestSuite
