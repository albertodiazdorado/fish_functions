testSuite "The helper function _create_AUR_directory"
	
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    functions -q _create_AUR_directory
	    and functions -e _create_AUR_directory
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/update-aur.fish"
	end
	
	function tearDown
	    set HOME $OLD_HOME
	    set -q OLD_HOME
	    and set -e OLD_HOME
	end
	
	fishTest "fails if the answer is different from y or Y"
		assertFails 1 "echo 'n' | _create_AUR_directory"
	endTest
	
	fishTest "fails if mkdir cannot create the folder"
		# given
		set -l AUR_FOLDER $HOME/non/existing/path
		
		# when -> then
		assertFails 1 "echo 'y' | _create_AUR_directory $AUR_FOLDER"
		
		# tear down
		set -q AUR_FOLDER
		and set -e AUR_FOLDER
	endTest
	
	fishTest "_create_AUR_directory correctly creates a folder if path exists"
		# given
		set -l AUR_FOLDER $HOME/existing_path
		
		# when
		assertSuccess "echo 'y' | _create_AUR_directory $AUR_FOLDER"
		
		# then
		assertSuccess test -d $AUR_FOLDER
		
		# tear down
		assertSuccess rm -r $AUR_FOLDER
		assertSuccess test ! -d $AUR_FOLDER
		set -q AUR_FOLDER
		and set -e AUR_FOLDER
	endTest
	
endTestSuite

testSuite "When passing options to update-aur, it"
	
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    functions -q update-aur && functions -e update-aur
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/update-aur.fish"
	    set -g READ_LOG $HOME/read_history.txt
	end
	
	function tearDown
	    set HOME $OLD_HOME
	    set -q OLD_HOME && set -e OLD_HOME
	    test -f $READ_LOG && rm $READ_LOG
	end
	
	fishTest "fails if the specified user does not exist"
		assertFails 1 update-aur -u imaginary_user
	endTest
	
	fishTest "correctly creates the AUR folder for the specified user"
		# given
		set -g TEST_USER_FOLDER $HOME/test_user
		function getent
		    echo $TEST_USER_FOLDER
		end
		assertSuccess mkdir $TEST_USER_FOLDER
		
		# when
		assertSuccess update-aur --user test_user --yes
		
		# then
		assertSuccess test -d $TEST_USER_FOLDER/build
		
		# tear down
		functions -e getent
		assertSuccess rm -r $TEST_USER_FOLDER
	endTest
	
	fishTest "accepts and uses an optional AUR folder"
		# when
		assertSuccess update-aur --aur custom_aur_dir --yes
		
		# then
		assertSuccess test -d $HOME/custom_aur_dir
		
		# tear down
		assertSuccess rm -r $HOME/custom_aur_dir
	endTest
	
	fishTest "accepts and uses a custom makepkg.conf file"
		# given
		touch "$HOME/.my_makepkg.conf"
		
		# when
		assertSuccess update-aur --config="$HOME/.my_makepkg.conf" --yes
		
		# then
		assertSuccess test -d $HOME/build
		
		# tear down
		assertSuccess rm -r $HOME/build
		assertSuccess rm $HOME/.my_makepkg.conf
	endTest
	
	fishTest "update-aur uses a custom makepkg.conf file in the home folder of the user, if the file exists"
		# given
		touch $HOME/.makepkg.conf
		
		# when
		assertSuccess "update-aur --config --yes"
		
		# then
		assertSuccess test -d $HOME/build
		
		# tear down
		assertSuccess rm -r $HOME/build
		assertSuccess rm $HOME/.makepkg.conf
	endTest
	
endTestSuite
