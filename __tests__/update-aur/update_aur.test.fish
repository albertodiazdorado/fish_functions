testSuite "The function update-aur"
	
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    set -g GIT_LOG $HOME/git_history.txt
	    set -g MAKEPKG_LOG $HOME/makepkg_log.txt
	    set -g TEST_BUILD_DIR $HOME/build
	    mkdir $TEST_BUILD_DIR
	    mkdir $TEST_BUILD_DIR/repository_1
	    mkdir $TEST_BUILD_DIR/repository_2
	    functions -q update-aur && functions -e update-aur
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/update-aur.fish"
	end
	
	function tearDown
	    set HOME $OLD_HOME
	    set -q OLD_HOME && set -e OLD_HOME
	    rm -r $TEST_BUILD_DIR
	    test -f $GIT_LOG && rm $GIT_LOG
	    test -f $MAKEPKG_LOG && rm $MAKEPKG_LOG
	end
	
	fishTest "excludes packages if the pull fails"
		# given
		function git
		    if test "$argv" = "fetch --all"
		        echo "git fetch --all @"(pwd) >>$GIT_LOG
		    else if test "$argv" = "pull"
		        echo "git pull @"(pwd) >>$GIT_LOG
		        if string match -r repository_1 (pwd) >/dev/null
		            echo "Not already up to date"
		            return 1
		        else
		            echo "Already up to date."
		        end
		    else if test "$argv" = "clean -fdx"
		        echo "git clean -fdx @"(pwd)
		    end
		end
		
		function makepkg
		    echo "WRONG LINE" >>$GIT_LOG
		    return 255
		end
		
		# when
		assertSuccess update-aur
		
		# then
		set -l expected_lines "git fetch --all @$TEST_BUILD_DIR/repository_1" \
		    "git pull @$TEST_BUILD_DIR/repository_1" \
		    "git fetch --all @$TEST_BUILD_DIR/repository_2" \
		    "git pull @$TEST_BUILD_DIR/repository_2" \
		
		set -l idx 1
		while read line
		    assertEqual "$expected_lines[$idx]" "$line"
		    set idx (math $idx+1)
		end <$GIT_LOG
		
		# tear down
		functions -q git && functions -e git
		functions -q makepkg && functions -e makepkg
	endTest
	
	fishTest "correctly install packages"
		# given
		function git
		    if test "$argv" = "fetch --all"
		        echo "git fetch --all @"(pwd) >>$GIT_LOG
		    else if test "$argv" = "pull"
		        echo "git pull @"(pwd) >>$GIT_LOG
		        if string match -r repository_1 (pwd) >/dev/null
		            echo "Already up to date."
		        else
		            echo "Pulling..."
		        end
		    else if test "$argv" = "clean -fdx"
		        echo "git clean -fdx @"(pwd) >>$GIT_LOG
		    else if test "$argv" = "config pull.rebase true"
		        echo "git config pull.rebase true @"(pwd) >>$GIT_LOG
		    else
		        echo "WRONG LINE: $argv" >>$GIT_LOG
		    end
		end
		
		function makepkg
		    echo "makepkg -sric @"(pwd) >>$MAKEPKG_LOG
		end
		
		# when
		assertSuccess update-aur --force
		
		# then
		set -l expected_git_log \
		    "git config pull.rebase true @$TEST_BUILD_DIR/repository_1" \
		    "git fetch --all @$TEST_BUILD_DIR/repository_1" \
		    "git pull @$TEST_BUILD_DIR/repository_1"\
		 "git clean -fdx @$TEST_BUILD_DIR/repository_1"\
		 "git config pull.rebase true @$TEST_BUILD_DIR/repository_2" \
		    "git fetch --all @$TEST_BUILD_DIR/repository_2" \
		    "git pull @$TEST_BUILD_DIR/repository_2"\
		 "git clean -fdx @$TEST_BUILD_DIR/repository_2"\
		
		set idx 1
		while read line
		    assertEqual "$expected_git_log[$idx]" "$line"
		    set idx (math $idx+1)
		end <$GIT_LOG
		
		set -l expected_makepkg_log "makepkg -sric @$TEST_BUILD_DIR/repository_1" "makepkg -sric @$TEST_BUILD_DIR/repository_2"
		set idx 1
		while read line
		    assertEqual "$expected_makepkg_log[$idx]" "$line"
		    set idx (math $idx+1)
		end <$MAKEPKG_LOG
		
		# tear down
		functions -q git && functions -e git
		functions -q makepkg && functions -e makepkg
	endTest
	
	fishTest "correcly excludes packages"
		# given
		function git
		    if test "$argv" = "fetch --all"
		        echo "git fetch --all @"(pwd) >>$GIT_LOG
		    else if test "$argv" = "pull"
		        echo "git pull @"(pwd) >>$GIT_LOG
		        echo "Already up to date."
		    end
		end
		
		# when
		assertSuccess update-aur --exclude repository_1
		
		# then
		set -l expected_lines "git fetch --all @$TEST_BUILD_DIR/repository_2" \
		    "git pull @$TEST_BUILD_DIR/repository_2"
		
		set -l idx 1
		while read line
		    assertEqual "$expected_lines[$idx]" "$line"
		    set idx (math $idx+1)
		end <$GIT_LOG
		
		# tear down
		functions -q git && functions -e git
	endTest
	
endTestSuite
