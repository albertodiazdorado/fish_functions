testSuite "The helper function _create_directory"
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    functions -q _create_directory && functions -e _create_directory
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/import-functions.fish"
	end
	
	function tearDown
	    set -g HOME $OLD_HOME
	    set -q OLD_HOME && set -e OLD_HOME
	end
	
	fishTest "returns 0 on succesful directory creation"
		set -l NEW_DIR "$HOME/new_dir"
		assertSuccess "echo y | _create_directory $NEW_DIR"
		assertSuccess test -d $NEW_DIR
		assertSuccess rm -r $NEW_DIR
		assertSuccess test ! -d $NEW_DIR
		set -q NEW_DIR
		and set -e NEW_DIR
	endTest
	
	fishTest "returns 1 if the user does not answer \"yes\" to the prompt"
		set -l NEW_DIR "$HOME/new_dir"
		assertFails 1 "echo n | _create_directory $NEW_DIR"
		assertSuccess test ! -d $NEW_DIR
	endTest
	
	fishTest "returns 1 if mkdir fails"
		set -l NEW_DIR "$HOME/a/b/c"
		assertFails 1 "echo y | _create_directory $NEW_DIR"
		assertSuccess test ! -d $NEW_DIR
	endTest
endTestSuite
