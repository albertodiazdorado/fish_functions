testSuite "import_functions"
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)"/user_folder"
	    set -g TEST_FISH_FOLDER "$HOME/.config/fish/functions"
	    set -q import-functions && set -e import-functions
	    mkdir -p $TEST_FISH_FOLDER
	    echo 'Hello world!' >$TEST_FISH_FOLDER/import-functions.fish
	    touch $TEST_FISH_FOLDER/outsider.txt
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/import-functions.fish"
	end
	
	function tearDown
	    set -x HOME $OLD_HOME
	    set -q OLD_HOME && set -e OLD_HOME
	end
	
	fishTest "fails if unknown arguments are used"
		assertFails 1 "import-functions --help --unknonw_arg"
	endTest
	
	fishTest "fails if the copy command fails"
		# given
		set -g failCode 123
		function cp
		    return $failCode
		end
		# Need to import the function again after redefining cp
		set -q import-functions && set -e import-functions
		set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
		source "$import_dir[1]/functions/import-functions.fish"
		
		# when -> then
		assertFails "$failCode" import-functions
		
		# tear down
		functions -q cp && functions -e cp
		set -q failCode && set -e failCode
		assertSuccess rm -r $HOME
		assertSuccess test ! -d $HOME
	endTest
	
	fishTest "pulls the newest functions without overriding, then cleans up"
		# when
		assertSuccess import-functions
		
		# then
		set -l expectedFiles "cheatsheet.pdf" "clip.fish" "g.fish" "gi.fish" "import-functions.fish" "init-latex.fish" "minikube-on.fish" "template.tex" "update-aur.fish" "vigila.fish"
		set -l actualFiles (ls $TEST_FISH_FOLDER)
		assertEqual (count $actualFiles) (math (count $expectedFiles) + 1)
		for file in $expectedFiles
		    assertSuccess contains "$file" "$actualFiles"
		end
		assertSuccess test -f $TEST_FISH_FOLDER/outsider.txt
		assertSuccess "test 'Hello world!' = (cat $TEST_FISH_FOLDER/import-functions.fish)"
		assertSuccess count (ls -A $HOME)
		
		# tear down
		assertSuccess rm -r $HOME
		assertSuccess test ! -d $HOME
	endTest
	
	fishTest "pulls the newest functions after cleaning existing ones, then cleans up"
		# when
		assertSuccess import-functions -c
		
		# then
		set -l expectedFiles "cheatsheet.pdf" "clip.fish" "g.fish" "gi.fish" "import-functions.fish" "init-latex.fish" "minikube-on.fish" "template.tex" "update-aur.fish" "vigila.fish"
		set -l actualFiles (ls $TEST_FISH_FOLDER)
		assertEqual (count $actualFiles) (count $expectedFiles)
		for file in $expectedFiles
		    assertSuccess contains "$file" "$actualFiles"
		end
		assertSuccess test ! -f $TEST_FISH_FOLDER/outsider.txt
		assertFails 1 "test 'Hello world!' = (cat '$TEST_FISH_FOLDER/import-functions.fish')"
		assertSuccess count (ls -A $HOME)
		
		# tear down
		assertSuccess rm -r $HOME
		assertSuccess test ! -d $HOME
	endTest
	
	fishTest "pulls the newest functions and overrides existing ones, then cleans up"
		# when
		assertSuccess import-functions -o
		
		# then
		set -l expectedFiles "cheatsheet.pdf" "clip.fish" "g.fish" "gi.fish" "import-functions.fish" "init-latex.fish" "minikube-on.fish" "template.tex" "update-aur.fish" "vigila.fish"
		set -l actualFiles (ls $TEST_FISH_FOLDER)
		assertEqual (count $actualFiles) (math (count $expectedFiles) + 1)
		for file in $expectedFiles
		    assertSuccess contains "$file" "$actualFiles"
		end
		assertSuccess test -f $TEST_FISH_FOLDER/outsider.txt
		assertFails 1 "test 'Hello world!' = (cat '$TEST_FISH_FOLDER/import-functions.fish')"
		assertSuccess count (ls -A $HOME)
		
		# tear down
		assertSuccess rm -r $HOME
		assertSuccess test ! -d $HOME
	endTest
endTestSuite
