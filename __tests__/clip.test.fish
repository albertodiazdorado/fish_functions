testSuite "The function clip"
	
	function setUp
	    set -g TEST_DIR (pushd (dirname (status filename)); and pwd; and popd)/"clip_test_dir"
	    functions -q clip
	    and functions -e clip
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 1 "/"; and popd)
	    source "$import_dir[1]/functions/clip.fish"
	end
	
	function tearDown
	    set -q TEST_DIR
	    and set -e TEST_DIR
	end
	
	fishTest "copies the output of a command and pastes it to the clipboard"
		assertEqual "mkdir: created directory '$TEST_DIR'" (clip mkdir -v $TEST_DIR)
		assertSuccess test -d $TEST_DIR
		assertEqual "mkdir: created directory '$TEST_DIR'" (xclip -o -selection c)
		assertEqual "removed directory '$TEST_DIR'" (clip rm -rv $TEST_DIR)
		assertSuccess test ! -d $TEST_DIR
		assertEqual "removed directory '$TEST_DIR'" (xclip -o -selection c)
	endTest
	
	fishTest "silently copies the output of a command and pastes it to the clipboard"
		assertEmpty (clip -s mkdir -v $TEST_DIR)
		assertSuccess test -d $TEST_DIR
		assertEqual "mkdir: created directory '$TEST_DIR'" (xclip -o -selection c)
		assertEmpty (clip -s rm -rv $TEST_DIR)
		assertSuccess test ! -d $TEST_DIR
		assertEqual "removed directory '$TEST_DIR'" (xclip -o -selection c)
	endTest
	
endTestSuite
