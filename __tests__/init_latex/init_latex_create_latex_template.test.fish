testSuite "The function init-latex"
	function setUp
	    functions -q init-latex
	    and functions -e init-latex
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/init-latex.fish"
	    set -g CURRENT_DIR (pushd (dirname (status filename)); and pwd; and popd)
	    set -g LATEX_PROJECT $CURRENT_DIR/latex_project
	    mkdir $LATEX_PROJECT
	
	end
	
	function tearDown
	    test -d $LATEX_PROJECT
	    and rm -r $LATEX_PROJECT
	end
	
	fishTest "correctly creates a latex file in the current folder"
		assertSuccess pushd $LATEX_PROJECT
		assertSuccess init-latex
		assertSuccess test -f main.tex
		assertEqual "644 main.tex" (stat -c "%a %n" main.tex)
		assertSuccess sha256sum -c $CURRENT_DIR/template.sha256
		assertSuccess popd
	endTest
	
	fishTest "correctly creates a latex file in the specified folder"
		assertSuccess init-latex -d $LATEX_PROJECT
		assertSuccess test -f "$LATEX_PROJECT/main.tex"
		assertSuccess pushd $LATEX_PROJECT
		assertEqual "644 main.tex" (stat -c "%a %n" "main.tex")
		assertSuccess sha256sum -c $CURRENT_DIR/template.sha256
		assertSuccess popd
	endTest
	
	fishTest "asks the user to create the specified dir, if it does not exist"
		assertSuccess rm -r $LATEX_PROJECT
		assertSuccess "echo y | init-latex -d $LATEX_PROJECT"
		assertSuccess test -f "$LATEX_PROJECT/main.tex"
		assertSuccess pushd $LATEX_PROJECT
		assertEqual "644 main.tex" (stat -c "%a %n" "main.tex")
		assertSuccess sha256sum -c $CURRENT_DIR/template.sha256
		assertSuccess popd
	endTest
	
	fishTest "fails if the specified dir does not exist, and the user does not want to create it"
		assertSuccess rm -r $LATEX_PROJECT
		assertSuccess "echo n | init-latex -d $LATEX_PROJECT"
		assertSuccess test ! -d "$LATEX_PROJECT"
	endTest
endTestSuite
