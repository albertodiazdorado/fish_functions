testSuite "The helper function _check_git_availability"
	function setUp
	    functions -q _check_git_availability
	    and functions -e _check_git_availability
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/init-latex.fish"
	end
	
	fishTest "returns 0 if git and gi are installed"
		function git
		    if test "$argv" = "--help"
		        return
		    end
		    return 1
		end
		function gi
		end
		assertSuccess _check_git_availability
		functions -e gi git _check_git_availability
	endTest
	
	fishTest "returns 1 if git is not installed"
		function git
		    return 1
		end
		assertFails 1 _check_git_availability
		functions -e git _check_git_availability
	endTest
	
	fishTest "returns 2 if git is installed but gi is not"
		function git
		    if test "$argv" = "--help"
		        return
		    end
		    return 1
		end
		functions -q gi
		and functions -e gi
		assertFails 2 _check_git_availability
		functions -e git _check_git_availability
	endTest
	
	fishTest "returns 2 if git and gi are installed but there is no internet connectivity"
		function git
			if test "$argv" = "--help"
				return
			end
			return 1
		end
		function gi
		end
		function nc
			return 1
		end
		assertFails 2 _check_git_availability
		functions -e gi git _check_git_availability
	endTest
endTestSuite


testSuite "The helper function _open_cheatsheet"
	function setUp
	    set -g OLD_HOME $HOME
	    set HOME (pushd (dirname (status filename)); and pwd; and popd)
	    set -g OPEN_LOG $HOME/open_log.txt
	    function open
	        echo "open $argv" >>$OPEN_LOG
	    end
	    functions -q _open_cheatsheet
	    and functions -e _open_cheatsheet
	    set -l import_dir (pushd (dirname (status filename)); and pwd | string split -r -m 2 "/"; and popd)
	    source "$import_dir[1]/functions/init-latex.fish"
	end
	
	function tearDown
	    set HOME $OLD_HOME
	    set -q OLD_HOME
	    and set -e OLD_HOME
	    set -q OPEN_LOG
	    and set -e OPEN_LOG
	    functions -q open
	    functions -e open
	end
	
	fishTest "opens the cheatsheet in the browser"
		assertSuccess touch $OPEN_LOG
		assertSuccess _open_cheatsheet any_dir browser
		while read line
		    assertEqual "open https://wch.github.io/latexsheet/latexsheet-a4.pdf" "$line"
		end <$OPEN_LOG
		assertSuccess rm $OPEN_LOG
		assertSuccess test ! -f $OPEN_LOG
	endTest
	
	fishTest "opens the cheatsheet locally"
		set -l CONFIG_DIR $HOME/config_dir
		assertSuccess mkdir $CONFIG_DIR
		assertSuccess touch $CONFIG_DIR/cheatsheet.pdf
		assertSuccess _open_cheatsheet $CONFIG_DIR many useless args
		while read line
		    assertEqual "open $CONFIG_DIR/cheatsheet.pdf" "$line"
		end <$OPEN_LOG
		assertSuccess rm $OPEN_LOG
		assertSuccess test ! -f $OPEN_LOG
		assertSuccess rm -r $CONFIG_DIR
		assertSuccess test ! -d $CONFIG_DIR
		set -q CONFIG_DIR
		and set -e OPEN_LOG
	endTest
	
	fishTest "correctly downloads and opens the cheatsheet"
		set -l CONFIG_DIR $HOME/config_dir
		assertSuccess mkdir $CONFIG_DIR
		assertSuccess _open_cheatsheet $CONFIG_DIR many useless args
		while read line
		    assertEqual "open $CONFIG_DIR/cheatsheet.pdf" "$line"
		end <$OPEN_LOG
		pushd $CONFIG_DIR
		assertSuccess sha256sum -c $HOME/cheatsheet.sha256
		popd
		assertSuccess rm $OPEN_LOG
		assertSuccess test ! -f $OPEN_LOG
		assertSuccess rm -r $CONFIG_DIR
		assertSuccess test ! -d $CONFIG_DIR
		set -q CONFIG_DIR
		and set -e CONFIG_DIR
	endTest
endTestSuite
